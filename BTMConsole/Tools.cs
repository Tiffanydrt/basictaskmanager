using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

namespace BTMConsole
{
    public  class Tools
    {
        private static List<string> MaListe;     
        private static string ChaineFusionnee;
        public Tools()
        {   
            //this.ChaineFusionnee=ChaineFusionnee;
            Tools.MaListe= new List<string>();
            
        }
        public string FusionnerChaine(Tasks Task1)
        {
            StringBuilder mabellechaine=new StringBuilder();
            mabellechaine.AppendFormat("{0};{1};{2};{3}",Task1.GetId(), Task1.GetNom(), Task1.GetDescription(), Task1.GetDeadline() ).ToString(); 

            SetChaineFusionnee(mabellechaine.ToString());
            return mabellechaine.ToString(); 
        }
        public void EcrireFichier(Tasks Task1)
        {    
            String path = @"Agenda.csv";
            string mabellechaine = FusionnerChaine(Task1);
            using (StreamWriter file = new StreamWriter(path, true) ) 
             {
                file.WriteLine(mabellechaine);
             }
        }

        public void CreerTache()
        {   
          string nom=" ", description= " ", date=" ";
          int id=0;
          int ajoutTache=1;
       
            while(ajoutTache==1)
            { 
                nom = SaisieNom(nom);
                description = SaisieDesc(description);
                date = SaisieDate(date);
                Tasks Task1=new Tasks(id, nom,description,date);
                Task1.SetId(Task1.NextId());
                MaListe.Add(FusionnerChaine(Task1)); //Ajoute la tache + fusionne en une string
                EcrireFichier(Task1);
                Console.WriteLine("Voulez vous ajouter une autre tache ? 1:oui 0:non");
                ajoutTache=int.Parse(Console.ReadLine());
            }  
        }

        public void VerifierDate()
        {
            //string date;
            //date = "16/05/2019 00:00:00";
            // string temps2;

            DateTime temps = DateTime.Today;

            foreach (string line in MaListe)
            {
                string[] words = line.Split(';');

                DateTime dateVerifier = Convert.ToDateTime(words[3]);
                int result = DateTime.Compare(dateVerifier, temps);
                if (result == 0)
                {
                    Console.WriteLine("La date limite du projet {0} est aujourd'hui", words[1]);
                }
                else if (result < 0)
                {
                    Console.WriteLine("La date du projet {0} est dépassée.", words[1]);
                }
            }

            //A faire : Tester + Rajouter les appels + Demander s'il veut ressaisir une date ou pas.
        }
        public string SaisieNom(string nom)
        {
               Console.WriteLine("Entrez le nom de la tâche");
                nom = Console.ReadLine();
                return nom; 
        }
        public string SaisieDesc(string description)
        {
                Console.WriteLine("Entrez la description de la tâche");
                description = Console.ReadLine();
                return description;  
        }

       /* public string SaisieDate(string date)
        {
                Console.WriteLine("Entrez la deadline format dd/mm/aaaa");
                date = Console.ReadLine(); 
                return date;
        }*/
         public void LectureFichier() //lire les lignes de la liste & affiche
        {
            int cpt=0;
            foreach(string line in MaListe) //Affichage de la liste
            {   
                cpt++;
                Console.WriteLine("Ligne {0} : {1}",cpt, line); //ligne par ligne
            }
        }

        public void InitListe() //lit le fichier pour ajouter les lignes à la liste
        {
            String path = @"Agenda.csv"; 
            string[] fichier = File.ReadAllLines(path);
            foreach(string line in fichier)
            {   
                 MaListe.Add(line);
            }
        }

        public void SuppressionTache2()
        {   
            String path = @"Agenda.csv";
            LectureFichier();
     
            int ligneChoisie;
            Console.WriteLine("Quelle ligne voulez-vous supprimer ?");
            ligneChoisie=int.Parse(Console.ReadLine());
            Console.WriteLine("La ligne {0} : {1} a bien été supprimée.",ligneChoisie, MaListe[ligneChoisie-1]);
            MaListe.RemoveAt(ligneChoisie-1); 
            
            using (StreamWriter file = new StreamWriter(path))
            {
                foreach(string line in MaListe)
                {
                    file.WriteLine(line);
                }
            }           
        }

        public string FusionnerDate(int jour, int mois, int annee)
        {
            StringBuilder dateFusion = new StringBuilder();
            dateFusion.AppendFormat("{0}/{1}/{2}", jour, mois, annee).ToString();
            return dateFusion.ToString();
        }

        public string SaisieDate(string date)
        {
            int jour = 0, mois = 0, annee = 0;
            DateTime Date = DateTime.Today;
            int AnneeEnCours = Date.Year;

            Console.WriteLine("Entrez le mois");
            mois = int.Parse(Console.ReadLine());
            while ((mois > 12) || (mois < 1))
            {
                Console.WriteLine("Entrez le mois");
                mois = int.Parse(Console.ReadLine());
            }
            if ((mois == 12) || (mois == 10) || (mois == 08) || (mois == 07) || (mois == 05) || (mois == 3) || (mois == 1))
            {
                while ((jour < 1) || (jour > 31))
                {
                    Console.WriteLine("Entrez le jour");
                    jour = int.Parse(Console.ReadLine());
                }
            }

            if ((mois == 04) || (mois == 06) || (mois == 09) || (mois == 11))
            {
                while ((jour < 1) || (jour > 30))
                {
                    Console.WriteLine("Entrez le jour");
                    jour = int.Parse(Console.ReadLine());
                }
            }

            if ((mois == 02))
            {
                while ((jour < 1) || (jour > 28))
                {
                    Console.WriteLine("Entrez le jour");
                    jour = int.Parse(Console.ReadLine());
                }
            }

            Console.WriteLine("Entrez l'Année");
            annee = int.Parse(Console.ReadLine());

            while ((annee < AnneeEnCours))
            {
       
                Console.WriteLine("Entrez l'annee");
                annee = int.Parse(Console.ReadLine());
             
            }
            date = FusionnerDate(jour, mois, annee);
            Console.WriteLine(date);
            return date;
        }


       

        public void ModifierChaine()
        {
            string nom = " ", description = " ", date = " ";
            int id = 0, ligneChoisie = -1, choixChamps = 0, autreChamps = 1;
            String path = @"Agenda.csv";
            string ligneRecuperee = " ";

            LectureFichier(); //Afficher la liste des taches 
            Console.WriteLine("Quelle ligne souhaitez-vous modifier ?");
            ligneChoisie = int.Parse(Console.ReadLine());
            //VerificationSaisie(ligneChoisie); A faire 
            Console.WriteLine(MaListe[ligneChoisie - 1].ToString()); //Affiche la ligne qu'on veut modif 
            ligneRecuperee = MaListe[ligneChoisie - 1]; //Recupère la ligne à modifier
            string[] words = MaListe[ligneChoisie - 1].Split(';');//Split la ligne à modifier
            id = int.Parse(words[0]);//Affecte l'id de la ligne à modifier dans l'id de la nouvelle ligne
            string[] nouvelleValeur = new string[] { words[1], words[2], words[3]};

            while (autreChamps==1)
            {
                Console.WriteLine("Quel(s) champs souhaitez-vous modifier ? \n 1 : Tous \n 2 : Nom \n 3 : Description \n 4 : Date \n");
                choixChamps = int.Parse(Console.ReadLine());

                switch (choixChamps)
                {
                    case 1:
                            nouvelleValeur[0] = SaisieNom(nom);
                            nouvelleValeur[1] = SaisieDesc(description);
                            nouvelleValeur[2] = SaisieDate(date);
                            break;
                    case 2:         
                            nouvelleValeur[0] = SaisieNom(nom);
                            break;
                    case 3:
                            nouvelleValeur[1] = SaisieDesc(description);
                            break;
                    case 4:

                            nouvelleValeur[2] = SaisieDate(date);
                            break;

                    default:
                        Console.WriteLine("Veuillez entrer une valeur entière entre 1 et 4");
                        break;
                }
             Console.WriteLine("Souhaitez-vous modifier un autre champs ? 1 : Oui 2 : Non ");
             autreChamps = int.Parse(Console.ReadLine());

            }
            
            Tasks task1 = new Tasks(id, nouvelleValeur[0], nouvelleValeur[1], nouvelleValeur[2]);//Création de l'instance task1
            FusionnerChaine(task1);
            string MaString = Tools.GetChaineFusionnee(); //Recupère la string fusionnée
            MaListe.Insert(ligneChoisie-1, MaString);   //insert la chaine qu'on veut
            MaListe.RemoveAt(ligneChoisie);
            LectureFichier();
            using (StreamWriter file = new StreamWriter(path))
            {
                foreach (string line in MaListe)
                {
                    file.WriteLine(line);
                }
            }
        }

        public static string GetChaineFusionnee()
        {
            return ChaineFusionnee;
        }

        public void SetChaineFusionnee(string ChaineFusionnee)
        {
            Tools.ChaineFusionnee=ChaineFusionnee;
        }
        public List <string> GetMaListe()
        {
            return MaListe;
        }

      
    }
}
