﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using BTMConsole;
using System.Threading.Tasks;

namespace Projet_ProgWin
{
    class Program
    { 
        static void Main(string[] args)
        {  
            int continuer=1;
            Console.WriteLine("Bienvenue dans le Basic Task Manager !");  
            Tools agenda=new Tools();
            agenda.InitListe();
            agenda.VerifierDate();

            while (continuer==1)
            {
                Console.WriteLine("Que souhaitez-vous faire ? \n 1 : Creer une tache \n 2 : Supprimer une tache \n 3 : Modifier une tache \n 4 : Afficher la liste des taches ");
                int choixMenu=0;
                choixMenu=int.Parse(Console.ReadLine());

                switch (choixMenu)
                {
                    case 1:
                        agenda.CreerTache();
                        break;
                    case 2:
                        agenda.SuppressionTache2();
                        break;
                    case 3:
                        agenda.ModifierChaine();  //A finir
                        break;
                    case 4:
                        agenda.LectureFichier();
                        break;
                    default:
                        Console.WriteLine("Veuillez entrer une valeur entiere entre 1 et 4");
                        break;
                }

                Console.WriteLine("Souhaitez-vous faire autre chose ? \n 1 : Oui \n 2 : Non");
                continuer=int.Parse(Console.ReadLine());
            }       
        }
    }
}