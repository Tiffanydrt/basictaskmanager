using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

namespace BTMConsole
{
    public class Tasks
    {
        private string nom;
        private string description;
        private string deadline;
        private int id;

        public Tasks(int id, string nom, string description, string deadline)
        {
            this.nom = nom;
            this.description = description;
            this.deadline = deadline;
            this.id = id;    
        }
        public int NextId()
        { 
            List<int> ListeID = new List<int>();
            String path = @"Agenda.csv"; 
            string[] lines2 = File.ReadAllLines(path);
            foreach(string line in lines2)
            {
                string[] words = line.Split(';');
                ListeID.Add(int.Parse(words[0]));
            }
             return ListeID.Max()+1;
        }

        public string GetNom()
        {
            return nom;
        }

        public void SetNom(string nom)
        {
            this.nom=nom;
        }
        public string GetDescription()
        {
            return description;
        }
        public void SetDescription(string description)
        {
            this.description=description;
        }
        public string GetDeadline()
        {
            return deadline;
        }
        public void SetDeadline(string deadline)
        {
         this.deadline=deadline;
        }

        public int GetId()
        {
            return id;
        }

        public void SetId(int id)
        {
            this.id = id;
        }
    }
 }